FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/kubernetes_demo-1.0.jar kubernetes_demo-1.0.jar
WORKDIR /opt/app
ENTRYPOINT ["java","-jar","/kubernetes_demo-1.0.jar"]
EXPOSE 8080